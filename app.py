import numpy as np
from flask import Flask, request, jsonify, render_template
import pickle
import model

app = Flask(__name__)
model = pickle.load(open('model.pkl', 'rb'))

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/predict',methods=['POST'])
def predict():
    '''
    For rendering results on HTML GUI
    '''
    import array

    features = []
   
    for x in request.form.values():
        features.append(x)

    print("**************************")
    print(features)
    print("**************************")

    # final_features = [np.array(int_features)]

    cat = features[0]
    doc = features[1]
    err = features[2]
    errDet = features[3]

    input = testDataPrepareWithEncode(cat, doc, err, errDet)
    prediction = model.predict(input)

    output = prediction[0]

    # return render_template('index.html', prediction_text='Please perform the following step to solve the issue: {}'.format(output))
    return output
@app.route('/predict_api',methods=['POST'])
def predict_api():
    '''
    For direct API calls trought request
    '''
    data = request.get_json(force=True)
    prediction = model.predict([np.array(list(data.values()))])

    output = prediction[0]
    return jsonify(output)

def testDataPrepareWithEncode(cat, doc, err, errDet):
    import csv
    import pandas as pd
    with open(r'sneha.csv', 'a', newline='') as csvfile:
        fieldnames = ['Ticket_Category','Doc_type', 'Error_type', 'Error_detail']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writerow({'Ticket_Category':cat, 'Doc_type':doc, 'Error_type':err, 'Error_detail':errDet})
        
    path ="sneha.csv"
    esd_ = pd.read_csv(path, delimiter=',', engine='python')

    import category_encoders as ce

    encoder_tc = ce.BinaryEncoder(cols=['Ticket_Category'])
    df_tc_copied = encoder_tc.fit_transform(esd_)

    encoder_et = ce.BinaryEncoder(cols=['Error_type'])
    df_et_copied = encoder_et.fit_transform(df_tc_copied)

    encoder_ed = ce.BinaryEncoder(cols=['Error_detail'])
    df_ed_copied = encoder_ed.fit_transform(df_et_copied)

    encoder_dt = ce.BinaryEncoder(cols=['Doc_type'])
    df_dt_copied = encoder_dt.fit_transform(df_ed_copied)

    df_dt_copied = df_dt_copied.drop(['Resolution'],axis='columns')
    final_input = df_dt_copied.tail(1)

    return(final_input)



if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)