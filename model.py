# # Importing the libraries
# import numpy as np
# import matplotlib.pyplot as plt
# import pandas as pd
# import pickle

# dataset = pd.read_csv('hiring.csv')

# dataset['experience'].fillna(0, inplace=True)

# dataset['test_score'].fillna(dataset['test_score'].mean(), inplace=True)

# X = dataset.iloc[:, :3]

# #Converting words to integer values
# def convert_to_int(word):
#     word_dict = {'one':1, 'two':2, 'three':3, 'four':4, 'five':5, 'six':6, 'seven':7, 'eight':8,
#                 'nine':9, 'ten':10, 'eleven':11, 'twelve':12, 'zero':0, 0: 0}
#     return word_dict[word]

# X['experience'] = X['experience'].apply(lambda x : convert_to_int(x))

# y = dataset.iloc[:, -1]

# #Splitting Training and Test Set
# #Since we have a very small dataset, we will train our model with all availabe data.

# from sklearn.linear_model import LinearRegression
# regressor = LinearRegression()

# #Fitting model with trainig data
# regressor.fit(X, y)

# # Saving model to disk
# pickle.dump(regressor, open('model.pkl','wb'))

# # Loading model to compare the results
# model = pickle.load(open('model.pkl','rb'))
# print(model.predict([[2, 9, 6]]))



####################################################################################################

# Importing the libraries
import pandas as pd
import numpy as np
import seaborn as  sns
import matplotlib.pyplot as plt
from sklearn_pandas import CategoricalImputer
import os as os
import category_encoders as ce
from sklearn.metrics  import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score
from sklearn.metrics import matthews_corrcoef
from sklearn.externals import joblib
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score
from sklearn.metrics import matthews_corrcoef
import pickle
import warnings
warnings.filterwarnings("ignore")

# Reading Dataset

# esd_df = pd.read_csv('/Users/encoreai/Desktop/EsdDataSet.csv', encoding='iso-8859-1', sep=',', engine='python')
esd_df = pd.read_csv('EsdDataSet_1.csv', encoding='iso-8859-1', sep=',', engine='python')

# Pre-Processing

# esd_array = esd_df['Doc_type'].values
# imputer = CategoricalImputer()
# imputer.fit_transform(esd_array)

esd_df["Doc_type"].fillna("No document type", inplace = True)
esd_df["Resolution"].fillna("A ticket will be raised for this issue", inplace = True)
esd_df["Error_detail"].fillna("No detail", inplace = True) 

# esd_df=esd_df.drop(["Doc_type"],axis=1)
# esd_df['Doc_type'] = esd_array

esd = esd_df.copy()

#Converting words to integer values

encoder_tc = ce.BinaryEncoder(cols=['Ticket_Category'])
df_tc = encoder_tc.fit_transform(esd)
        
encoder_et = ce.BinaryEncoder(cols=['Error_type'])
df_et = encoder_et.fit_transform(df_tc)
        
        
encoder_ed = ce.BinaryEncoder(cols=['Error_detail'])
df_ed = encoder_ed.fit_transform(df_et)
        
encoder_dt = ce.BinaryEncoder(cols=['Doc_type'])
df_dt = encoder_dt.fit_transform(df_ed)

# Next step is creating training and testing datasets:

x=df_dt.drop(['Resolution'],axis='columns')
x.shape

y=df_dt['Resolution']
y.shape

x_train,x_test,y_train,y_test=train_test_split(x,y,test_size=0.2,random_state=1)

# print(x_train.shape,x_test.shape,y_train.shape,y_test.shape)

# Model implementation

RFClassifier=RandomForestClassifier(criterion='entropy',n_estimators=100,max_features=3,oob_score=True,bootstrap=True,n_jobs=-1,random_state=1)

#Model fit
RFClassifier.fit(x_train,y_train)


# to predict a single value
# row = x_test.head(1)
# print(row)

RFClassifier_pred=RFClassifier.predict(x_test)

# Finding Accracy Score
# print('Accuracy Score:',accuracy_score(y_test,rf1_pred))

# Matthews Corealation Coefficient 
# mcc = matthews_corrcoef(y_test,RFClassifier_pred)
# print('Matthews_corrcoef for Model is:',mcc)

#Feature importances
features=df_dt.columns[[0,1,2,3,4,5,6,7,8,9,10,11,12,13,15,16,17,18,19,20,21,22,23,24]]
importances = RFClassifier.feature_importances_
indices = np.argsort(importances)

plt.figure(1)
plt.title('Feature Importances')
plt.barh(range(len(indices)), importances[indices], color='b', align='center')
plt.yticks(range(len(indices)), features[indices])
plt.xlabel('Relative Importance')

oob_error=1-RFClassifier.oob_score_
# print(oob_error)    #0.150


# Finding best parameters using GridCVSearch
params={
            'criterion':['gini','entropy'],
            'n_estimators':[50],
            'max_features':[2,3,4,5,6,7,8],
        }

rf_gridcv=GridSearchCV(estimator=RFClassifier,cv=5,param_grid=params,scoring='accuracy')
rf_grid=rf_gridcv.fit(x_train,y_train)

# print(rf_gridcv.best_params_)

y_predrf=rf_gridcv.predict(x_test)

# Getting one row for input to the model

x = x_test.head(1)

# Making a pickel file of the model

pickle.dump(rf_gridcv, open('model.pkl','wb'))

model = pickle.load(open('model.pkl','rb'))
print(model.predict(x))
print("***********************************************")




